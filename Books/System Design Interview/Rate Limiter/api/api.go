package api

import (
	"net/http"
	"encoding/json"
	"server/limiter"
)

type API struct {
	limiter limiter.Limiter
}

type Response struct {
	Status string `json:"status"`
}

func (a *API) StartServer(port string) error {
	mux := http.NewServeMux()
	mux.HandleFunc("/api/hitThis", a.limiter.Limit(a.HandleAPICall))
	return http.ListenAndServe(port, mux)
}

func (a *API) HandleAPICall(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&Response{"success"})
}

func NewAPI() *API {
	l := limiter.NewTokenBucketLimiter(10, 5.0)
	return &API{l}
}	
