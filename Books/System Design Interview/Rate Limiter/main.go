package main

import (
	"server/api"
	"fmt"
)

func main() {
	errCh := make(chan error)
	api := api.NewAPI()
	go func() {
		errCh <- api.StartServer(":8081")
	}()
	err := <- errCh
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("welp")
}
