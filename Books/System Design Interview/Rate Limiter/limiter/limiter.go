package limiter

import (
	"net/http"
)

const (
	LimiterError = "Limit Reached"
)

type Limiter interface {
	Limit (http.HandlerFunc) http.HandlerFunc
}

type LeakingBucket struct {}
type FixedWindow struct {}
type SlidingWindowLog struct {}
type SlidingWindowCounter struct {}

type Response struct {
	Remaining int `json:"remaining"`
}
