package limiter

import (
	"net"
	"net/http"
	"log"
	"errors"
	"time"
	"strconv"
)

type bucket struct {
	remaining int
	timestamp time.Time
}

type TokenBucketLimiter struct {
	bucketSize float64
	refillRate float64
	buckets map[string]map[string]bucket
}

func (t *TokenBucketLimiter) Limit(next http.HandlerFunc) http.HandlerFunc {	
	return func (w http.ResponseWriter, r *http.Request) {
		i, retry, err := t.checkLimit(r)
		if i > 0 && err == nil {
			w.Header().Add("X-Ratelimit-Limiting", strconv.Itoa(i))
			w.Header().Add("X-Ratelimit-Limit", strconv.FormatFloat(t.bucketSize, 'E', -1, 64))
			w.Header().Add("X-Ratelimit-Retry-After", strconv.FormatFloat(retry.Seconds(), 'E', -1, 64))
			next(w, r)
		} else {
			http.Error(w, LimiterError, http.StatusTooManyRequests)
		}
	}
}				

func (t *TokenBucketLimiter) refillBucket(ip string, url string) bucket {
	b, ok := t.buckets[ip][url]
	if !ok {
		return bucket{int(t.bucketSize - 1.0), time.Now()}
	} else if b.remaining > 0 {
		return bucket{int(b.remaining - 1.0), time.Now()}
	} else {
		duration := time.Now().Sub(b.timestamp)
		log.Printf("Duration: %f", duration.Seconds())
		if (duration.Seconds() >= t.refillRate) {
			return bucket{int(t.bucketSize - 1.0), time.Now()}
		}
		return b
	}
}

func (t *TokenBucketLimiter) checkLimit(r *http.Request) (int, time.Duration, error) {
	log.Printf("IP addr: %s", r.RemoteAddr)
	log.Printf("URL: %s", r.URL)
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	url := r.URL.String()
	if err != nil { 
		return -1, -1, nil
	}
	if t.buckets[ip] == nil {
		t.buckets[ip] = make(map[string]bucket)
	}
	t.buckets[ip][url] = t.refillBucket(ip, url)
	log.Printf("%v", t.buckets)
	remain := t.buckets[ip][url].remaining
	retry := time.Until(t.buckets[ip][url].timestamp)
	if remain > 0 {
		return remain, retry, nil
	}
	return -1, -1, errors.New(LimiterError)
}
/*
func (t *TokenBucketLimiter) Remaining(r *http.Request) int {
	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	url := r.URL.String()
	return t.buckets[ip][url].remaining
}
*/
func NewTokenBucketLimiter(bucketSize float64, refillRate float64) Limiter {
	return &TokenBucketLimiter{bucketSize, bucketSize, make(map[string]map[string]bucket)}
}
