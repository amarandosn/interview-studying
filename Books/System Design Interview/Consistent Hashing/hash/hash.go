package hash

import (
	"crypto/sha1"
	"crypto/rand"
	"io"
	"fmt"
	"sort"
	"errors"
)

type hash interface {
	AddServer()
	RemoveServer(int) error
	GetData(string) (interface{}, error)
	SetData(string, interface{}) error
	RemoveData(string) error
	GetCountOnlineServers() int
}

type Server struct {
	addr [sha1.Size]byte
	data map[[sha1.Size]byte]interface{} 
}

type Hash struct {
	servers []*Server
}

func NewConsistentHash(serverCount int) (*Hash) {
	hash := Hash{}
	for i := 0; i < serverCount; i++ {
		hash.AddServer()
	}
	return &hash
}

func (s Server) String() string {
	res := "["
	for k,v := range s.data {
		res += fmt.Sprintf("%v:%v", k, v)
	}
	res += "]"
	return res
}

func (h Hash) String() string {
	res := "["
	for i, s := range h.servers {
		res += fmt.Sprintf("Server %v: %v", i, s)
	}
	res += "]"
	return res
}	

func (h *Hash) AddServer() {
	random := make([]byte, 10)
	rand.Read(random)
	addr := sha1.Sum(random)
	data := make(map[[sha1.Size]byte]interface{})
	h.servers = append(h.servers, &Server{addr, data})
	sort.Slice(h.servers, func(i, j int) bool {
		return compareServers(h.servers[i], h.servers[j])
	})
}

func compareAddr(a, b [sha1.Size]byte) bool {
	for n := 0; n < sha1.Size; n++ {
		if a[n] != b[n] {
			return a[n] < b[n]
		}
	}
	return false
}

func compareServers(i, j *Server) bool {
	return compareAddr(i.addr, j.addr)
}

func (h *Hash) RemoveServer(i int) error { 
	if i >= len(h.servers) {
		return errors.New("There aren't that many servers")
	}	
	s := h.servers[i]
	next, err := h.getNextServer(s.addr)
	fmt.Printf("curr: %v, next server: %v\n", i, next)
	if err != nil {
		return err
	}
	// data has already been hashed and there shouldnt be any collisions 
	for k, v := range s.data {
		next.data[k] = v
	}
	h.servers = append(h.servers[:i], h.servers[i+1:]...)
	return nil	
}

func (h *Hash) getNextServer(addr [sha1.Size]byte) (*Server, error) {
	i := 0
	if len(h.servers) == 0 {
		return nil, errors.New("There are no servers online")
	}
	// while addr is greater than the next server, increment
	for !compareAddr(addr, h.servers[i].addr) {
		if i + 1 >= len(h.servers){
			break
		}
		i += 1	
	}
	if h.servers[i].addr == addr {
		i = (i + 1) % len(h.servers)
	}
	return h.servers[i], nil
}

func (h *Hash) GetData(key string) (interface{}, error) {
	var res interface{}
	hasher := sha1.New()
	io.WriteString(hasher, key)
	var hashKey [sha1.Size]byte
	copy(hashKey[:], hasher.Sum(nil))
	s, err := h.getNextServer(hashKey)
	if err != nil {
		return res, err
	}
	res, ok := s.data[hashKey]
	if !ok {
		return res, errors.New("Server does not have key")
	}
	return res, nil
}

func (h *Hash) SetData(key string, value interface{}) error {
	hasher := sha1.New()
	io.WriteString(hasher, key)
	var hashKey [sha1.Size]byte
	copy(hashKey[:], hasher.Sum(nil))
	s, err := h.getNextServer(hashKey)
	if err != nil {
		return err
	}
	s.data[hashKey] = value
	return nil
}

func (h *Hash) RemoveData(key string) error {
	hasher := sha1.New()
	io.WriteString(hasher, key)
	var hashKey [sha1.Size]byte
	copy(hashKey[:], hasher.Sum(nil))
	s, err := h.getNextServer(hashKey)
	if err != nil {
		return err
	}
	delete(s.data, hashKey)
	return nil
}

func (h *Hash) GetCountOnlineServers() int {
	return len(h.servers)
}
