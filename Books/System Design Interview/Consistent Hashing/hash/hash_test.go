package hash

import (
	"testing"
)

func TestAddServer(t *testing.T) {
	h := NewConsistentHash(4)
	t.Logf("%v\n", h)
	h.AddServer()
	if h.GetCountOnlineServers() != 5 {
		t.Fatalf("Server addition failed")
	}
	t.Logf("%v\n", h)	
}	

func TestRemoveServer(t *testing.T) {
	h := NewConsistentHash(4)
	h.SetData("hello", "world")
	// server locations are psuedo random; will have to find where that ended up
	var found int
	for i, server := range h.servers {
		if len(server.data) > 0 {
			found = i
		}
	}
	t.Logf("%v", h)
	err := h.RemoveServer(found)	
	t.Logf("%v", h)
	if err != nil {
		t.Fatalf("Unable to remove in bounds server")
	}
	var found2 int
	for i, server := range h.servers {
		if len(server.data) > 0 {
			found2 = i
		}
	}
	if found2 != found % len(h.servers) {
		t.Fatalf("Data is not going to the correct backup server")
	}
	err = h.RemoveServer(1)
	if err != nil {
		t.Fatalf("Unable to remove in bounds server")
	}
	err = h.RemoveServer(100)
	if err == nil {
		t.Fatalf("Error index out of bounds not thrown")
	}
	h = NewConsistentHash(0)
	err = h.RemoveServer(1)
	if err == nil {
		t.Fatalf("Error index out of bounds not thrown")
	}
}

func TestSetData(t *testing.T) {
	h := NewConsistentHash(4)
	err := h.SetData("hello", "world")
	if err != nil {
		t.Fatalf("Error setting data to consistent hash")
	}
	t.Logf("%v\n", h)
}	

func TestGetData(t *testing.T) {
	h := NewConsistentHash(4)
	err := h.SetData("hello", "world")
	if err != nil {
		t.Fatalf("Error setting data to consistent hash")
	}
	data, err := h.GetData("hello")
	if err != nil {
		t.Fatalf("Error retrieving data")
	}
	if data != "world" {
		t.Fatalf("Error retrieving correct data")
	}
}

func TestRemoveData(t *testing.T){
	h := NewConsistentHash(4)
	h.SetData("hello", "world")
	err := h.RemoveData("hello")
	if err != nil {
		t.Fatalf("Error on removing data from nonempty hash")
	}
	for _, server := range h.servers {
		if len(server.data) > 0 {
			t.Fatalf("Error removing data from hash")
		}
	}
}

func TestGetCountOnlineServers(t *testing.T) {
	h := NewConsistentHash(4)
	if len(h.servers) != 4 {
		t.Fatalf("Incorrect number of servers allocated")
	}
}
