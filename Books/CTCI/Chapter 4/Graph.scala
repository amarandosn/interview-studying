import collection.mutable.ArrayBuffer
import collection.mutable.Queue
import collection.mutable.HashSet

class Node[T](var data: T, val neighbors: ArrayBuffer[Node[T]] = ArrayBuffer[Node[T]]()):
	def getNeighbors = neighbors
	def getData = data
	def addNeighbor(node: Node[T]) = this.neighbors += node
	def removeNeighbor(node: Node[T]) = this.neighbors -= node

	def bfs: String =
		val sb = StringBuilder()
		val next = Queue[Node[T]](this)
		val visited = HashSet[Node[T]]()

		while next.size > 0 do
			val node: Node[T] = next.dequeue
			if !visited.contains(node) then
				next.enqueueAll(node.neighbors)
				sb.append(node.data)
				visited += node

		sb.toString

	def dfs: String = 
		def loop(node: Node[T], visited: HashSet[Node[T]], sb: StringBuilder): String =
			if !visited.contains(node) then
				visited += node
				sb.append(node.data)
				for neighbor <- node.neighbors do
					loop(neighbor, visited, sb)	
			sb.toString

		loop(this, HashSet[Node[T]](), StringBuilder())

@main def main() = 
	val graph1 = Node[Int](1, ArrayBuffer[Node[Int]](Node[Int](2), Node[Int](3, ArrayBuffer[Node[Int]](Node[Int](4)))))
	println(graph1.bfs)
	println(graph1.dfs)
