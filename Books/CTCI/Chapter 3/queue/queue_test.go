package queue

import (
	"testing"
)

func TestEnqueue(t *testing.T){
	q := Queue{}
	q.Enqueue(1)
	if q.head.val != 1 {
		t.Fatalf("Head not being set correctly on enqueue empty queue")
	}
	if q.tail.val != 1 {
		t.Fatalf("Tail not being set correctly on enqueue emtpy queue")
	}	
	q.Enqueue(2)
	if q.head.val != 1 {
		t.Fatalf("Head not being set correctly on enqueue nonempty queue")
	}
	if q.tail.val != 2 {
		t.Fatalf("Tail not being set correctly on enqueue nonemtpy queue")
	}	
}

func TestDequeue(t *testing.T){
	q := Queue{&Node{nil, 1}, &Node{nil, 1}}
	val, err := q.Dequeue()
	if val != 1 {
		t.Fatalf("Incorrect value on dequeing nonempty queue")
	}
	if err != nil {
		t.Fatalf("Error returned on dequeueing nonempty queue")
	}
	_, err = q.Dequeue()
	if err == nil {
		t.Fatalf("No error on dequeueing empty queue")
	}
}

func TestPeek(t *testing.T){
	q := Queue{&Node{nil, 1}, &Node{nil, 1}}
	val, err := q.Peek()
	if val != 1 {
		t.Fatalf("Incorrect value being returned by Peet()")
	}
	if err != nil {
		t.Fatalf("Error being retured on nonemtpy queue Peek()")
	}
	q = Queue{}
	_, err = q.Peek()
	if err == nil {
		t.Fatalf("No Error being returned on empty queue Peek()")
	}
}

func TestIsEmpty(t *testing.T){
	q := Queue{&Node{nil, 1}, &Node{nil, 1}}
	val := q.IsEmpty()
	if val == true {
		t.Fatalf("IsEmpty returuning true on nonempty queue")
	}
	q = Queue{}
	val = q.IsEmpty()
	if val == false {
		t.Fatalf("IsEmpty returning false on empty queue")
	}
}
