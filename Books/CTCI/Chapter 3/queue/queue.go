package queue

import (
	"fmt"
	"errors"
)

type queueType int
var nilType queueType
var emptyQueue = errors.New("Queue is empty")

type queue interface {
	Enqueue(queueType)
	Dequeue() (queueType, error)
	Peek() (queueType, error)
	IsEmpty() bool
}

type Queue struct {
	head *Node
	tail *Node
}

type Node struct {
	next *Node
	val 	queueType
}

func (q *Queue) Enqueue(val queueType) {
	n := &Node{nil, val}
	if q.tail != nil {
		q.tail.next = n
	}
	q.tail = n
	if q.head == nil {
		q.head = q.tail
	}
}

func (q *Queue) Dequeue() (queueType, error) {
	if q.IsEmpty() {
		return nilType, emptyQueue
	}
	val := q.head.val
	q.head = q.head.next
	if q.head == nil {
		q.tail = q.head
	}
	return val, nil
}

func (q *Queue) Peek() (queueType, error) {
	if q.IsEmpty() {
		return nilType, emptyQueue
	}
	return q.head.val, nil
}

func (q *Queue) IsEmpty() bool {
	return q.head == nil
}

func (q Queue) String() string {
	res := "["
	for curr := q.head; curr != nil; curr = curr.next {
		if curr.next != nil {
			res += fmt.Sprintf("%v, ", curr.val)
		} else {
			res += fmt.Sprintf("%v", curr.val)
		}
	}
	res += "]"
	return res
}
