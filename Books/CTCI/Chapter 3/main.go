package main

import (
	"fmt"
	"chapter-3/stacksOnStacks"
)

func main() {
	fmt.Println("welcome to chapter 3. Its all about stacks and queues.")
	fmt.Println("run 'go test chapter-3/[queue | stack | stacksOnStacks | MyQueue ]' to test implementations")
	ss := stacksOnStacks.StacksOnStacks{}
	fmt.Println(ss)
}
