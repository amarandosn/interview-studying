package stack

import (
	"fmt"
	"errors"
)

var nilType int

type stack interface {
	Pop () (int, error)
	Push(int)  
	Peek(int, error)
	IsEmpty() bool
}

type Stack struct {
	head *Node
}

type Node struct {
	data int
	next *Node
}

var emptyStack = errors.New("Stack is empty")

func (s *Stack) Pop() (int, error) {
	if s.IsEmpty() {
		return nilType, emptyStack
	}
	temp := s.head.data
	s.head = s.head.next
	return temp, nil
}


func (s *Stack) Push(data int) {
	s.head = &Node{data, s.head}
}

func (s *Stack) Peek() (int, error) {
	if s.IsEmpty() {
		return nilType, emptyStack
	}
	return s.head.data, nil
}

func (s *Stack) IsEmpty() bool {
	return s.head == nil
}

func (s Stack) String() string {
	res := "["
	curr := s.head
	for curr != nil {
		if curr.next != nil {
			res += fmt.Sprintf("%v, ", curr.data)
		} else {
			res += fmt.Sprintf("%v", curr.data)
		}
		curr = curr.next
	}
	res += "]"
	return res
}
