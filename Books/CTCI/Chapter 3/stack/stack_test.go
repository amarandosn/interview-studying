package stack

import (
	"testing"
)

func TestPop(t *testing.T) {
	s := Stack{&Node{1, nil}}
	num, err := s.Pop()
	if err != nil {
		t.Fatalf("Pop throwing unexpected err")
	}
	if num != 1 {
		t.Fatalf("Pop not returning the correct value")
	}
	_, err = s.Pop()
	if err == nil {
		t.Fatalf("Emtpy stack should return an error on pop")
	}
}

func TestPush(t *testing.T) {
	s := Stack{&Node{1, nil}}
	s.Push(2)
	if s.head.data != 2 {
		t.Fatalf("Push not correctly pushing")
	}
}

func TestPeek(t *testing.T) {
	s := Stack{&Node{1, nil}}
	peek, err := s.Peek()
	if peek != 1 {
		t.Fatalf("Peek not returning the correct value")
	}
	if err != nil {
		t.Fatalf("Peek returning unexpected error")
	}
	s = Stack{}
	peek, err = s.Peek()
	if err == nil {
		t.Fatalf("Peek not throwing error on empty stack")
	}
}

func TestIsEmtpy(t *testing.T) {
	s := Stack{&Node{1, nil}}
	empty := s.IsEmpty()
	if empty == true {
		t.Fatalf("IsEmpty returning true on a non empty stack")
	}
	s = Stack{}
	empty = s.IsEmpty()
	if empty == false {
		t.Fatalf("IsEmpty returning false on an empty stack")
	}
}
