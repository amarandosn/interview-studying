Keep track of min element with a pointer. Check incoming push to see if less than the min. If so set the pointer to that. 
I used a linked-list to implement the stack, where this is easy. 
If you used an array to implement the stack, it would work similarly; keep track of the index. When new pushes come in; if greater than min; increment index. if found new min set index = 0. 
	- when removing just slice the array at that index and return the element. 

- Both methods take O(1) insertion and O(1) retrieval
