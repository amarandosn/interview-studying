package stacksOnStacks

import (
	"errors"
	"chapter-3/stack"
	"fmt"
)

type stacksonstacks interface {
	Pop() (int, error)
	Push (int)
	PopAt(int) (int, error)
}

type stacksOnStacksStruct struct {
	stacks []*stack.Stack
	threshold int
	count []int
}

func StacksOnStacks(threshold int) *stacksOnStacksStruct {
	ss := stacksOnStacksStruct{}
	ss.stacks = make([]*stack.Stack,0)
	ss.threshold = threshold
	ss.count = make([]int, 0)
	return &ss
}

func (ss *stacksOnStacksStruct) Pop() (int, error) {
	i := len(ss.stacks)
	if i > 0 {
		i -= 1
		s := ss.stacks[i]
		res, err := s.Pop()
		if err != nil {
			return -1, err
		}
		ss.count[i]--
		if ss.count[i] <= 0 {
			ss.stacks = ss.stacks[:i]
		}
		return res, err
	} else {
		return -1, errors.New("StacksOnStacks is Empty")
	}
}

func (ss *stacksOnStacksStruct) Push(val int) {
	i := len(ss.stacks)
	if i == 0 || ss.count[i - 1] == ss.threshold {
		s := &stack.Stack{}
		s.Push(val)
		ss.stacks = append(ss.stacks, s)
		ss.count = append(ss.count, 1)
	} else {
		s := ss.stacks[i - 1]
		s.Push(val)
		ss.count[i - 1]++
	}
}

// TODO: this doesnt back fill the stack so the count is now off for that particular stack
// calling again will result in a the next element; i dont think this will be called on a empty stack. then there wouldnt be a stack in ss.stacks to begin with  
func (ss *stacksOnStacksStruct) PopAt(i int) (int, error) {
	if i >= len(ss.stacks) {
		return -1, errors.New("Index out of bounds")
	}
	res, err := ss.stacks[i - 1].Pop()
	if err != nil {
		return -1, err
	}
	ss.count[i - 1]--
	if ss.count[i - 1] == 0 {
		ss.stacks = append(ss.stacks[:i-1], ss.stacks[i+1:]...)
	}
	return res, nil
}

func (ss stacksOnStacksStruct) String() string {
	res := "["
	for _, s := range ss.stacks {
		res += fmt.Sprintf("%v", s)
	}
	return res + "]"
}
