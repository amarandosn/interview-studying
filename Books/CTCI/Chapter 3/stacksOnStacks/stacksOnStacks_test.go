package stacksOnStacks

import (
	"testing"
)

func TestPush(t *testing.T) {
	ss := StacksOnStacks(10)
	ss.Push(1)
	ss.Push(2)
	res, err := ss.stacks[0].Peek()
	if res != 2 {
		t.Fatalf("Unable to push correct value")
	}
	if err != nil {
		t.Fatalf("Unable to Peek correctly to stack")
	}
}

func TestPop(t *testing.T) {
	ss := StacksOnStacks(10)
	res, err := ss.Pop()
	if res != -1 {
		t.Fatalf("Returning wrong value on popping empty array")
	}
	if err == nil {
		t.Fatalf("Error not throw on popping empty array")
	}
	ss.Push(1)
	res, err = ss.Pop()
	if err != nil {
		t.Fatalf("Error thrown on nonempty StacksOnStacks Pop: %v", err)
	}
	if res != 1 {
		t.Fatalf("Wrong number returned on StacksOnStacks Pop")
	}
}

func TestPopAt(t *testing.T) {
	ss := StacksOnStacks(10)
	for i := 0; i < 19; i++ {
		ss.Push(i)
	}	
	res, err := ss.PopAt(1)
	if err != nil {
		t.Fatalf("Error on PopAt: %v", err)
	}
	if res != 9 {
		t.Fatalf("PopAt not returing the correct number.")
	}
}
