import collection.mutable.HashMap

// exprX = (exponent -> coefficient)

def addExpressions(expr1: HashMap[Double, Double], expr2: HashMap[Double, Double]): HashMap[Double, Double] = {
	val ans = HashMap[Double, Double]().addAll(expr1)
	for exp <- expr2.keys do
		ans.update(exp, ans.getOrElse(exp, 0.0) + expr2(exp))
	ans
}

@main def main() = {
	val expr1 = HashMap[Double, Double](2.0->3.0) // 3x^2
	val expr2 = HashMap[Double, Double](4.0->1.0, 2.0->2.0) // x^4 + 2x^2
	println(addExpressions(expr1, expr2)) // x^4 + 5x^2
}
