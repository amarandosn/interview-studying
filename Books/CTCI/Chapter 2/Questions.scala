import collection.mutable.HashSet
import annotation.tailrec

class Node(val data: Int, var next: Node = null):
	def tail: Node = 
		this.next 

	def addToTail(data: Int): Node = 
		def loop(node: Node, data: Int): Node = 
			if node == null then 
				Node(data)
			else
				Node(node.data, loop(node.next, data))
		Node(this.data, loop(this.next, data))

	def append(node: Node): Node = 
		def append(a: Node, b: Node): Node =
			if a != null then 
				Node(a.data, append(a.next, b))
			else if b != null then
				Node(b.data, append(b.next, null))
			else 
				null
		Node(this.data, append(this.next, node))

	def deleteValue(data: Int, occur: Int = 1): Node =
		def loop(prev: Node, curr: Node, data: Int, occur: Int): Node = 
			if curr == null then null
			else 
				if curr.data == data && occur > 0 then
					loop(prev, curr.next, data, occur - 1)
				else
					Node(curr.data, loop(curr, curr.next, data, occur))
		if this == null then null
		else
			if this.data == data && occur > 0 then
				loop(this, this.next, data, occur - 1)
			else 
				Node(this.data, loop(this, this.next, data, occur))

	override def toString: String = 
		@tailrec def loop(node: Node, sb: StringBuilder): String = 
			if node == null then 
				sb.toString
			else
				sb.append(node.data)
				if node.next != null then sb += ' '
				loop(node.next, sb)
		loop(this, StringBuilder())	

/** Question #1: Remove Duplicates
	* I added the occurences check
	* also assumed they wanted a new list, just following scala schtuff
	*/
	def removeDuplicates: Node =
		def loop(prev: Node, node: Node, set: HashSet[Int]): Node = 
			if node == null then node
			else 
				if set.contains(node.data) then 
					loop(prev, node.next, set)
				else
					Node(node.data, loop(node, node.next, set += node.data))
		Node(this.data, loop(this, this.next, HashSet[Int]()))	

/** Question #2: Find Kth-to-last Element in List
	* first thought, what if k > list.size? head
	* k is inclusive (4,5,6) where k = 3; ans = 4
	*/
	def kthToLast(k: Int): Node = 
		def loop(curr: Node, point: Node, dist: Int): Node = 
			if curr == null then point
			else if dist >= k then 
				loop(curr.next, point.next, dist)
			else 
				loop(curr.next, point, dist + 1)
		if this == null then this
		else loop(this.next, this, 1)

/** Question #3: Delete Middle Node 
	* Never the head or the tail, assume list.size >= 3
	* O(n) to run thru and count, and then run thru and delete
	* in keeping with immutable list, I will return a head pointer to a new list w/o the middle
	* i wonder if i can delete it coming back up the stack????? idk how to even do that would be sick tho
	*/ 
	def removeMiddle: Node = 
		var length = -1
		def loop(curr: Node, index: Int = 0): Node = 
			if curr.next == null then 
				length = index + 1
				curr
			else
				val node = Node(curr.data, loop(curr.next, index + 1))
				if index > 0 && index + 1 == length / 2 then
					node.next = node.next.next
				node
		loop(this, 0)

/** Question #4 Partition Linked List
	* so looks like the partition happens between 2, 10 and the 5s are in the right partition
	* thinking i need to go thru the list keeping track of 2 heads 1 for >= 1 for < partition
	* at the end, attach last value of < to the head of >=
	* deal! 
	*/
	
	def partition(part: Int): Node = 
		@tailrec def loop(curr: Node, greater: Node = Node(0), less: Node = Node(0)): Node =
			if curr == null then
				// attach last less to greater head
				less.next.append(greater.next)
			else
				if curr.data >= part then
					loop(curr.next, greater.addToTail(curr.data), less)
				else
					loop(curr.next, greater, less.addToTail(curr.data))
		loop(this)

def testQuestion(index: Int, tests: List[(String, String)]) = 
	print(s"Question ${index + 1}: ")
	for test <- tests 
	do
		assert(test._1 == test._2)
	println("Passed")

def unitTests(tests: List[List[(String, String)]]) = 
	for i <- tests.indices 
	do
		testQuestion(i, tests(i))
	println("All tests passed")
	
@main def main() = {
	val head = Node(1).addToTail(2).addToTail(3).addToTail(4).addToTail(5).addToTail(6).deleteValue(6)	
	val set = head.removeDuplicates
	val headMinusMiddle = head.removeMiddle
	val p1 = Node(10).addToTail(9).addToTail(7).addToTail(6).addToTail(5).addToTail(4).addToTail(3).addToTail(2).addToTail(1).addToTail(0)
	
	try{
		val tests = List(
			List((set.toString, "1 2 3 4 5")),
			List((head.kthToLast(3).toString, "3 4 5")),
			List((headMinusMiddle.toString, "1 2 4 5")),
			List((p1.partition(3).toString, "2 1 0 10 9 7 6 5 4 3"))
		)

		unitTests(tests)
	}
	catch{
		case e: AssertionError => println(e)
	}
}
