class Node(val data: Int, var next: Node = null):
	def tail: Node = this.next 

	def addToTail(data: Int) = 
		var point = this
		while point.next != null do
			point = point.next
		point.next = Node(data)
	

	def deleteValue(head: Node, v: Int): Node = 
		if head != null && head.data == v then 
			head.next
		else
			var point = head
			while point.next != null do 
				if point.next.data == v then
					point.next = point.next.next
				else
					point = point.next
			head
	
	
	override def toString: String = 
		var point = this
		val sb = StringBuilder()

		while point != null do
			sb.append(point.data)
			sb += ' '
			point = point.next
		sb.toString

	def weave(that: Node): Node = 
		def weave(_this: Node, _that: Node): Node = 
			if _this != null then 
				if _that != null then
					Node(_this.data, weave(_that.tail, this.tail))
				else
					Node(_this.data, weave(_this.tail, null))
			else
				if _that != null then
					Node(_that.data, weave(_that.tail, null))
				else
					null
		Node(this.data, weave(that, this.tail))

@main def main() = {
	val list = Node(1)
	list.addToTail(2)
	// list.addToTail(3)
	// list.addToTail(4)
	// list.addToTail(5)	
	println(list.toString)

	// list.deleteValue(head, 4)
	// println(list.toString)

	val list1 = Node(11)
	list1.addToTail(12)
	// list1.addToTail(13)
	// list1.addToTail(14)
	// list1.addToTail(15) 
	println(list1)
	
	val weave = list.weave(list1)
	println(weave.toString)
}
