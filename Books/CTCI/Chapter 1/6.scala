def compress(s: String): String = {
	def loop(s: String, i: Int, acc: StringBuilder, c: Char, cc: Int): String = {
		if i >= s.length then
			acc ++= c.toString ++= cc.toString
			val comp = acc.toString
			if comp.length == s.length then s else comp
		else
			if c == s(i) then
				loop(s, i+1, acc, c, cc+1)
			else
				loop(s, i+1, acc ++= c.toString ++= cc.toString, s(i), 1)
	}
	loop(s, 0, StringBuilder(), s(0), 0)
}

@main def main() = {
	assert("a2b1c5a3" == compress("aabcccccaaa"))
	println("test cases passed")
}
