package ArrayList 

import reflect.ClassTag // stores the erased class of a given ty[e T, accessible via runtimeClass field. This is particularly useful when instantiating Arrays whose elements types are unknown and compile time.

class ArrayList[A:ClassTag]:
	private var pointer: Int = 0
	private var size: Int = 2
	private var array = new Array[A](size)

	def apply(i: Int): Option[A] = {
		if i < pointer && i >= 0 then
			Some(array(i))
		else 
			None
	}

	def add(a: A) = {
		if pointer >= size-1 then
			array = array ++ new Array[A](size)
			size = size * 2 
		array(pointer) = a
		pointer = pointer + 1
	}

	def foldRight(a: A)(f: (A, A) => A): A = {
		array.foldRight(a)(f)
	}
def testArrayList() = {
		val a = new ArrayList[String]()
		print(a(1))
		a.add("hello")
		a.add("world")
		print(s"${a(0)}, ${a(1)}!")
}

@main def main() = {
	// testArrayList()
}
