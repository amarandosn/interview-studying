import annotation.tailrec

class Node[K,V](_next: Node[K,V] = null, _key: K, _value: V):
	var next: Node[K,V] = _next
	val key: K = _key
	val value: V = _value

class HashTable[K,V]:
	private val array = new Array[Node[K,V]](500)
	private def hash(key: K): Int = {
		key.toString().toList.sum % 500
	}

	def apply(key: K): Option[V] = {
		@annotation.tailrec
		def find(key: K, node: Node[K,V]): Option[Node[K,V]] = {
			if node != null && key == node.key then 
				Some(node)
			else if node.next != null then 
				find(key, node.next) 
			else None
		}
		find(key, array(hash(key))) match {
			case None => None
			case Some(node) => Some(node.value)
		}
	}

	def addOne(elem: (K, V)): HashTable[K,V] = {
		val head = array(hash(elem._1))

		@annotation.tailrec
		def getTail(head: Node[K,V]): Node[K,V] = {
			if head.next != null then
				getTail(head.next)
			else 
				head
		}

		val tail = new Node(null, elem._1, elem._2)
		if head == null then
			array(hash(elem._1)) = tail 
		else
			getTail(head).next = tail 

		this
	}

@main def testHashTable() = {
	val x = new HashTable[String, String]()
	x.addOne(("hello"->"world"))
	x("hello") match {
		case Some(v) => print(v)
		case None => print("not found")
	}
}
