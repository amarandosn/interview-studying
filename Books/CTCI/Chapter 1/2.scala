@main def main(s1: String, s2: String) = {
	println(s1.permutations.toList.filter(_ == s2).length > 0)
}	
