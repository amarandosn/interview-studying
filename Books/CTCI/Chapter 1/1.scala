import collection.mutable.Map

def allUnique(s: String): Boolean = {
	val map = Map[Char, Boolean]()
	def loop(s: String, i: Int): Boolean = {
		if i >= s.length then true
		else 
			map.get(s(i)) match {
				case None => {
					map(s(i)) = true
					loop(s,i+1)
				}
				case Some(_) => false
			}
	}
	loop(s, 0)
}

@main def main(s: String) = {
	println(allUnique(s))
}
