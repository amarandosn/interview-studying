import collection.mutable.Map	

def palindromePerm(s: String): Boolean = {
	var map = Map[Char, Int]()
	var _s = s.toLowerCase
	def loop(s: String, i: Int): Boolean = {
		if i >= s.length then 
			val values = map.values.toList
			values.count(_ == 2) == values.length - 1 && values.count(_ == 1) == 1	
		else
			if s(i).isLetter then 
				map.get(s(i)) match {
					case None => map(s(i)) = 1
					case Some(a: Int) => map(s(i)) = a + 1
				}
			loop(s, i+1)
	}
	loop(_s, 0)
}

@main def main() = {
	println(palindromePerm("Tact Coa"))
}
