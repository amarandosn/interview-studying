def urlify(s: String): String = {
	var sb = StringBuilder(s.length, "")
	def loop(s: String, i: Int, g: Boolean): String = {
		if i >= s.length then sb.toString()
		else
			if s(i) == ' ' && g then
				loop(s, i+1, g)
			else if s(i) == ' ' && !g then
				sb ++= "%20"
				loop(s, i+1, true)
			else
				sb.addOne(s(i))		
				loop(s, i+1, false)
	}
	loop(s, 0, false)
}

@main def tester() = {
	assert(urlify("hello   world") == "hello%20world")
	assert(urlify("   ") == "%20")
	println("Test cases passed!")
} 
