/** Assumptions
	* rotating clockwise
	* needs to be in place, mutable ArraySeqs 
	* gonna start with a 2x2 to get the general shape of the program then generalize
*/

import collection.mutable.ArraySeq

def rotateRight(matrix: ArraySeq[ArraySeq[Int]]): ArraySeq[ArraySeq[Int]] = {
	/* assume its a 2x2 to start
	[[1,2], will become [[4,1]
	 [4,3]] 						 [3,2]]
	*/

	// traverse bottom->top, left->right
	// keep track of temp variable for swap
	// make loops to go thru and print each one in the correct order

	def loop(x: Int, y: Int): Unit  = {
		if x < matrix(0).length then
			if y >= 0 then
				print(matrix(x)(y))
				loop(x,y-1)
			loop(x+1, matrix.length-1)
	}
	loop(0, matrix(0).length-1)
	matrix
}

@main def main() = {
	val matrix = ArraySeq(ArraySeq(1,2), ArraySeq(4,3))
	rotateRight(matrix)
}
