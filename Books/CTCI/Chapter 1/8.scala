// 0 matrix, if an element in an MxN array is 0 the entire column and row is set to 0


def zero(matrix: Array[Array[Int]], x: Int = 0, y: Int = 0): Array[Array[Int]] = {
	if x >= matrix.head.size && y >= matrix.size then matrix else
		if matrix[x][y] == 0 then
			matrix[x].fill(0)
			zero(matrix, x+1, 0)
		else
			zero(matrix, x, y+1)	
	
