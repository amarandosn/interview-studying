import collection.mutable.ArrayBuffer

def quicksort(arr: Array[Int]): Array[Int] = 
	Array(1)

def generateArray(n: Int): Array[Int] =
	val ab = ArrayBuffer[Int]()
	for i <- 0 until n do
		ab += (math.random * 100).toInt
	ab.toArray
		
@main def main(n: Int) = 
	val arr = generateArray(n)
	println(arr)
	assert(arr.sorted == quicksort(arr))
	println("Test passed!")
