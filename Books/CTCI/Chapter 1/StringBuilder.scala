package StringBuilder 


import reflect.ClassTag // stores the erased class of a given ty[e T, accessible via runtimeClass field. This is particularly useful when instantiating Arrays whose elements types are unknown and compile time.

class ArrayList[A:ClassTag]:
	private var pointer: Int = 0
	private var size: Int = 2
	private var array = new Array[A](size)

	def apply(i: Int): Option[A] = {
		if i < pointer && i >= 0 then
			Some(array(i))
		else 
			None
	}

	def add(a: A) = {
		if pointer >= size-1 then
			array = array ++ new Array[A](size)
			size = size * 2 
		array(pointer) = a
		pointer = pointer + 1
	}

	def foldRight(a: A)(f: (A, A) => A): A = {
		array.foldRight(a)(f)
	}

class StringBuilder():
	var array = new ArrayList[String]()

	def append(s: String): StringBuilder = {
		array.add(s)
		this
	}

	override def toString(): String = {
		array.foldRight("")((a, acc) => if a != null then a + acc else acc)
	}

@main def main() = {
	val s = new StringBuilder()
	s.append("Hello, ")
	s.append("world!")
	println(s.toString)
}
