def binarySearch(value: Int, arr: Array[Int]): Int =
	var left = 0
	var right = arr.size - 1

	while left <= right do
		val mid = left + (right - left) / 2
		if arr(mid) == value then
			left = mid
			right = -2
		else if arr(mid) < value then
			left = mid + 1
		else
			right = mid - 1

	if right == -2 then left else -1

@main def main() = 
	val arr = (1 to 100).toArray
	println(binarySearch(0, arr))
	println(binarySearch(1, arr))
	println(binarySearch(111, arr))
	println(binarySearch(3, arr))
