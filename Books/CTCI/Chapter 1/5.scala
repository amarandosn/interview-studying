def oneAway(s1: String, s2: String): Boolean = {
	// insert a character
	// remove a character
	// replace a character
	if s1 == s2 then true
	else
		val l1 = s1.toList
		s2.toList.filterNot(l1.contains(_)).length <= 1
}

@main def main() = {
	val tests = List(
		("pale", "ple", true),
		("pales", "pale", true),
		("pale", "bale", true),
		("pale", "bake", false)
	)

	assert(tests.forall((s1, s2, r) => oneAway(s1, s2) == r) == true)
	println("All tests passed")
}
