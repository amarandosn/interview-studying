package binarysearch

type First struct {}

func (f *First) Search(n int, slice []int) bool {
	low := 0
	high := len(slice) - 1
	for low <= high {
		m := low + (high - low) / 2
		if slice[m] == n {
			return true
		} else if slice[m] > n {
			high = m - 1
		} else if slice[m] < n {
			low = m + 1 
		}
	}
	return false
}

func NewFirst() *First {
	return &First{}
}
