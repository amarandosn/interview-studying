package binarysearch

import (
	"testing"
)

var implementations []BinarySearch = []BinarySearch{
	NewFirst(),
}

func TestBinarySearch(t *testing.T) {
	slice := []int{1,3,5,7,10,39,40,100,150,151,200,256,300,400}
	tests := []int{1,257,40,2,400}
	for _, bs := range implementations {
		t.Run("Test Search Output", func (t *testing.T) {
			val := []bool{}
			a := []bool{}
			for _, test := range tests {
				val = append(val, bs.Search(test, slice))
				a = append(a, answer(test, slice))
			}
			t.Logf("attempt: %v answer: %v", val, a)
			if same := compareSlices(val, a); !same {
				t.Fatalf("Nope, wrong algorithm")
			}
		})
	}
}

func answer(n int, slice []int) bool {
	low := 0
	high := len(slice) - 1
	for low <= high {
		m := low + (high - low) / 2
		if slice[m] == n {
			return true
		} else if slice[m] > n {
				high = m - 1
		} else if slice [m] < n {
				low = m + 1
		}
	}
	return false
}

func compareSlices (a, b []bool) bool {
	if len(a) != len(b) {
		return false
	}
	for i, e := range a {
		if e != b[i] {
			return false
		}
	}
	return true
}
