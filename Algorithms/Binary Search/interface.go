package binarysearch

type BinarySearch interface {
	Search(int, []int) bool
}
