package dfs

import "testing"

var implementations []DFS = []DFS{
	NewFirst(),
}

var A Node = Node{ "A", nil }
var B Node = Node{ "B", nil }
var C Node = Node{ "C", nil }
var D Node = Node{ "D", nil }
var E Node = Node{ "E", nil }
var F Node = Node{ "F", nil }

func setupGraph() {
	A.neighbors = []*Node{&B, &C}
	B.neighbors = []*Node{&D, &E, &F}
	C.neighbors = []*Node{&A, &D, &B}
	D.neighbors = []*Node{&F, &E}
	E.neighbors = []*Node{&C, &A, &F}
	F.neighbors = []*Node{&E, &B}
}

func TestDFS(t *testing.T) {
	setupGraph()
	var tests []*Node = []*Node{&A, &B, &C, &D, &E, &F}
	for _, dfs := range implementations {
		t.Run("Test Search Output", func (t *testing.T) {
			for i, test := range tests {
				val := dfs.Search(test)
				expected := answer(test)
				if val != expected {
					t.Fatalf("DFS failed on test: %v. Expected: %v Received: %v", i, expected, val)
				}
			}
		})
	}
}

func answer(node *Node) string {
	var res string
	var stack []*Node
	visited := make(map[*Node]bool)
	if node != nil {
		stack = append(stack, node)
	}
	for len(stack) > 0 {
		curr := stack[0]
		stack = stack[1:]
		if visited[curr] == false {
			visited[curr] = true	
			res += curr.value
			stack = append(curr.neighbors, stack...)
		}
	}
	return res
}
