package dfs

type First struct {}

func (f *First) Search(node *Node) string {
	var res string
	var stack []*Node
	visited := make(map[*Node]bool)
	if node != nil {
		stack = append(stack, node)
	}
	for len(stack) > 0 {
		curr := stack[0]
		stack = stack[1:]
		if visited[curr] == false {
			visited[curr] = true	
			res += curr.value
			stack = append(curr.neighbors, stack...)
		}
	}
	return res
}

func NewFirst() *First {
	return &First{}
}
