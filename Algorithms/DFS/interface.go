package dfs

type DFS interface {
	Search(*Node) string
}

type Node struct {
	value string
	neighbors []*Node
}
