/* Find the duplicate number
	Got it kind of in spirit after research; 
	Need to know Floyd's tortiose and hare alg 
		but also need it for arrays to each other and not just for linked lists
	1. Learn for linked list
	2. Learn for array of indexes
*/

case class Node(value: Int, var next: Node)


@annotation.tailrec def findCycle(fast: Node, slow: Node): Boolean = {
	if fast == slow then true
	else if fast == null || fast.next == null || slow == null then false
	else findCycle(fast.next.next, slow.next)
}

@main def main() = {
	val head = Node(0, null)
	var pointer = head
	val length = 10
	var cycle = head
	
	for index <- 1 until length do
		if index == length / 2 then
			cycle = Node(index, null)
			pointer.next = cycle
		else if index == length - 1 then
			pointer.next = cycle
		else 
			pointer.next = Node(index, null)
		pointer = pointer.next


	val head2 = Node(0, null)
	pointer = head2
	for index <- 1 until length do 
		pointer.next = Node(index, null)
		pointer = pointer.next

	println(findCycle(head.next, head))
	println(findCycle(head2.next, head2))
 
}
