func isReachableAtTime(sx int, sy int, fx int, fy int, t int) bool {
	// at first thinking depth first to get from start to finish
	// there is probably a closed equation for distance however and i can compare against t
	// ah its EXACTLY t seconds interesting
	// seems to be you can always grow to match t but if t is not long enough then false. So then i think if i find the minumum distance to the finish and if t > min then it should be true
	// ex (2,4) (7,7) probably pythagorean?
	// sqrt((7-2)^2 + (7-4)^2) is 6 so hey hey thats a great coincidence
	// dist := int(math.Sqrt(math.Pow(float64(fx - sx), 2) + math.Pow(float64(fy - sy),2)))
	n := int(math.Abs(float64(fy - sy)))
	m := int(math.Abs(float64(fx - sx)))
	dist := max(n, m)
	if dist == 0 && t == 1 {
		return false
	}
	return t >= dist
}