func getRow(rowIndex int) []int {
    // such a small row-index means we dont need the closed equation; although i know it exists for each row
    // "only" 33 rows down we can do a tab method or something
    slice := make([][]int, rowIndex + 1, rowIndex + 1)
    for i := 0; i <= rowIndex; i++ {
        row := make([]int, i + 1, i + 1)
        for j := 0; j < i + 1; j++ {
            if j == 0 || j == i {
                row[j] = 1
            } else {
                row[j] = slice[i-1][j-1] + slice[i-1][j]
            }
        }
        slice[i] = row
    }
    return slice[rowIndex]
}

