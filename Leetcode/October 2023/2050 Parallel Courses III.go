// Got a few test cases, correct. TODO: find the edge cases / bugs in this code
import "fmt"

func minimumTime(n int, relations [][]int, time []int) int {
    type data struct {
        children []int
        order int
    }
    graph := make(map[int]data)
    sources := []int{}
    if len(relations) == 0 {
        max := 0
        for _, weight := range time {
            if (weight > max) { max = weight }
        }
        return max
    }
    for _, vertex := range relations {
        p := vertex[0]
        n := vertex[1]
        prev, ok := graph[p]
        if !ok {
            graph[p] = data{[]int{n}, 0}
        } else {
            graph[p] = data{append(prev.children, n), prev.order}
        }
        next, ok := graph[n]
        if !ok {
            graph[n] = data{[]int{}, 1}
        } else {
            graph[n] = data{next.children, next.order + 1}
        }
    }
    for i, n := range graph {
        if n.order == 0 {
            sources = append(sources, i)
        }
    }
    var max int
    fmt.Printf("graph: %v\nsources: %v\ntime: %v\n", graph, sources, time)
    for _, s := range sources {
        visited := make(map[int]bool)
        stack := []int{s}
        weight := 0
        for len(stack) > 0 {
            curr := stack[0]
            stack = stack[1:]
            fmt.Printf("curr: %v: visited %v\n", curr, visited)
            if visited[curr] == false {
                visited[curr] = true
                weight += time[curr - 1]
                if len(graph[curr].children) > 0 {
                    stack = append(stack, graph[s].children...)
                } else {
                    if (weight > max){ // not checking to make sure all courses are completed
                        fmt.Printf("source: %v: weight %v\n", s, weight)
                        max = weight
                    }
                    visited[curr] = false
                    weight -= time[curr - 1]
                }
            }
        }
    }
    return max
}
