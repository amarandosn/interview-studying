func GetRoot(n int, leftChild *[]int, rightChild *[]int) int {
    // just get the number that doesnt show up in left/right
    x := -1
    for i, l := range (*leftChild) {
        r := (*rightChild)[i]
        x ^= l
        x ^= r
    }
    if x < 0 || x > n { 
        return 0
    } else {
        return x
    }
}

func validateBinaryTreeNodes(n int, leftChild []int, rightChild []int) bool {
    // first thought; cycle detection
        // hashset for cycles
        // dfs or bfs over the nodes
    // fails if n != map.size || cycle
    var curNode int
    set := make(map[int]bool)
    var stack []int

    stack = append(stack, GetRoot(n, &leftChild, &rightChild))
    if stack[0] == -1 { return false }
    
    for len(stack) > 0 {
        curNode = stack[0]
        stack = stack[1:] 
        if !set[curNode] {
            set[curNode] = true
        } else {
            return false
        }
        if leftChild[curNode] != -1 {
            stack = append(stack, leftChild[curNode])
        }
        if rightChild[curNode] != -1 {
            stack = append(stack, rightChild[curNode])
        }
    }
    return len(set) == n
}
