/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * type NestedInteger struct {
 * }
 *
 * // Return true if this NestedInteger holds a single integer, rather than a nested list.
 * func (this NestedInteger) IsInteger() bool {}
 *
 * // Return the single integer that this NestedInteger holds, if it holds a single integer
 * // The result is undefined if this NestedInteger holds a nested list
 * // So before calling this method, you should have a check
 * func (this NestedInteger) GetInteger() int {}
 *
 * // Set this NestedInteger to hold a single integer.
 * func (n *NestedInteger) SetInteger(value int) {}
 *
 * // Set this NestedInteger to hold a nested list and adds a nested integer to it.
 * func (this *NestedInteger) Add(elem NestedInteger) {}
 *
 * // Return the nested list that this NestedInteger holds, if it holds a nested list
 * // The list length is zero if this NestedInteger holds a single integer
 * // You can access NestedInteger's List element directly if you want to modify it
 * func (this NestedInteger) GetList() []*NestedInteger {}
 */

type NestedIterator struct {
    list []int
}

func flatten(nestedList []*NestedInteger, acc []int) []int {
    for _, c := range nestedList {
        if c.IsInteger(){
            acc = append(acc, c.GetInteger())
        } else {
            acc = append(acc, flatten(c.GetList(), make([]int, 0))...)
        }
    }
    return acc
}

func Constructor(nestedList []*NestedInteger) *NestedIterator {
    list := flatten(nestedList, make([]int, 0))
    return &NestedIterator{list}
}

func (this *NestedIterator) Next() int {
    var res int
    if (this.HasNext()) {
        res = this.list[0]
        this.list = this.list[1:]
    }
    return res
}

func (this *NestedIterator) HasNext() bool {
    return len(this.list) > 0
}
