class Solution(object):
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        lps = [0] * len(needle)
        prev, i = 0, 1
        while i < len(needle):
            if needle[prev] == needle[i]:
                prev += 1
                lps[i] = prev
                i += 1
            elif prev == 0:
                lps[i] = 0 
                i += 1
            else:
                prev = lps[prev - 1]
        i, j = 0, 0
        while i < len(haystack):
            if haystack[i] == needle[j]:
                i += 1
                j += 1
            elif j == 0:
                i += 1
            else:
                j = lps[j - 1]
            if j == len(needle): return i - j
        return -1
