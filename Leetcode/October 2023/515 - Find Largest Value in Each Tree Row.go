/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

type Node struct {
    TN *TreeNode
    Depth int
}

func largestValues(root *TreeNode) []int {
    if root == nil {
        return make([]int, 0)
    }
    var acc []int
    queue := []*Node{&Node{root, 0}}
    for len(queue) > 0 {
        n := queue[0]
        queue = queue[1:]
        if len(acc) <= n.Depth {
            acc = append(acc, n.TN.Val)
        } else if n.TN.Val > acc[n.Depth] {
            acc[n.Depth] = n.TN.Val
        }
        if n.TN.Left != nil {
            queue = append(queue, &Node{n.TN.Left, n.Depth + 1})
        }
        if n.TN.Right != nil {
            queue = append(queue, &Node{n.TN.Right, n.Depth + 1})
        }
    }
    return acc
}
