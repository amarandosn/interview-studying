class Solution(object):
    def majorityElement(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        # my answer:
            # hashmap (freq map) then hashset(answer) O (n) time and O (n) space
        # O(n) time and O(1) space
            # makes me think of treating nums as a stack/queue and popping (no)

        map = {}
        ans = set()
        cutoff = len(nums) / 3

        for n in nums:
            if n not in ans:
                freq = map.get(n, 0) + 1
                map[n] = freq
                if freq > cutoff:
                    ans.add(n)
        return ans 
