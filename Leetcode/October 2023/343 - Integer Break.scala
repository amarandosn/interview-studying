object Solution {
    import collection.mutable.ArrayBuffer
    def integerBreak(n: Int): Int = {
        def product(check: Int): Int =
            (0 until check).foldLeft(1) { case (acc, curr) =>
                acc * (n / check + (if (n % check > curr) 1 else 0))
            }

        (2 to n / 2 + 1).map(product).max
    }
}

// Time O(n^2)
// Space O(n)
