class Solution(object):
    def numIdenticalPairs(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        map = {}
        ans = 0
        for num in nums:
            if num in map:
                ans += map[num]
                map[num] += 1
            else:
                map[num] = 1
        return ans
