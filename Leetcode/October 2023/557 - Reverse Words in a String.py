class Solution:
    def reverseWords(self, s: str) -> str:
        return " ".join(map(lambda n: n[::-1], s.split()))
