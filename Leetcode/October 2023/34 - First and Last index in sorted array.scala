object Solution {
    def searchRange(nums: Array[Int], target: Int): Array[Int] = {
        // Binary search until found then 2 pointer in each direction 

        def binarySearch(nums: Array[Int], target: Int): Int = {
            var left = 0
            var right = nums.size - 1

            while (left <= right) {
                var mid = left + (right - left) / 2
                if (nums(mid) == target) {
                    return mid
                }
                else if (nums(mid) < target){
                    left = mid + 1
                }
                else {
                    right = mid - 1
                }
            }
            return -1
        }

        var idx = binarySearch(nums, target)
        if (idx == -1) { return Array(-1, -1) }

        var left = idx
        var right = idx
        println(left)
        while (idx != -1) {
            left = idx
            idx = binarySearch(nums.slice(0, left), target)
        }

        idx = right
        while (idx != -1) {
            right = idx
            idx = binarySearch(nums.slice(right + 1, nums.size), target)
        }

        Array(left, right)
    }
}
