object Solution {
    def isMonotonic(nums: Array[Int]): Boolean = {
        var inc = true
        var dec = true
        nums.dropRight(1).foldLeft(nums(nums.size - 1)){ (prev: Int, curr: Int) =>
            if (prev < curr){
                inc = false
            }
            // else if (prev <= curr){
            //     dec = false
            // } 
            curr
        }
        inc
    }
}

