# Interview Studying

Gotta get a job! 
Using this to also keep my vim/git skills fresh for industry.
Target: push daily on the weekdays and potentially on the weekends

## Project Structure

### Books

- Working through the classic coding study guide books, as well as some extra ones that catch my attention
- Implement stuff in go / scala, work chapter by chapter

### Leetcode

Account: https://leetcode.com/natesonthecase/

- Months / Day / <leetcode_number>.[scala | go]
    - Could be a python file too, will switch off to keep python skills up to date for programming interviews

### Algorithms

- Algorithm implementation practice. Setup testing environment for each alg
- structure
	- Algorithms/<algorithm>/interface.go
		- sets up an interface for the algorithm; what methods need to be implemented
	- Algorithms/<algorithm>/<algorithm>_test.go
		- tests the alg implementation i just wrote
	- Algorithsm/<algorithms>/the rest of the files
		- separate implementations that i wrote that day to study. Timstamped by git

### Architectures

- Going to practice Data Modeling and Code Design here 
